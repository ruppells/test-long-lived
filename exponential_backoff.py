#!/usr/bin/env python

import os
from twisted.internet import protocol, reactor, endpoints
from twisted.protocols.basic import LineReceiver
from datetime import datetime
import sys
from twisted.python import log
log.startLogging(sys.stdout)

class BackoffPing(LineReceiver):
    def _looping_backoff_ping(self, wait=1):
        minutes_since_last_ping = datetime.now() - self.time
        self.time = datetime.now()
        self.sendLine(
            "{}: time since last ping: {}, next ping in {} seconds".format(
                self.time.isoformat(), minutes_since_last_ping, wait))
        self.lc = reactor.callLater(wait, self._looping_backoff_ping, wait *2)
        log.msg("{}: ping, next scheduled for {}".format(self.peer_str, wait))
    def connectionMade(self):
        peer = self.transport.getPeer()
        self.peer_str = "{}:{}".format(peer.host, peer.port)
        log.msg("{}: connected".format(self.peer_str))
        self.time = datetime.now()
        self._looping_backoff_ping()
    def connectionLost(self, reason):
        log.msg("{}: disconnected".format(self.peer_str))
        self.lc.cancel()

class BackoffPingFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return BackoffPing()

endpoints.serverFromString(reactor, "tcp:{}".format(os.environ.get('RUPPELLS_SOCKETS_LOCAL_PORT', 1234))).listen(BackoffPingFactory())
reactor.run()
